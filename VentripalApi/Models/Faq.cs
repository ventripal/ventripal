﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VentripalApi.Models
{
    public class Faq
    {
        public int ScreenNo { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}