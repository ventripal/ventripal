﻿namespace VentripalApi.Models
{
    public class Wish
    {
        public string User { get; set; }
        public int ScreenNo { get; set; }
        public int StarRating { get; set; }
        public string Wishes { get; set; }
    }
}