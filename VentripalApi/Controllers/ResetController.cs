﻿using System.Web.Http;

namespace VentripalApi.Controllers
{
    public class ResetController : ApiController
    {
        // GET: api/Reset
        public string Get()
        {
            BillingController.Reset();
            ClientBillingController.Reset();
            WishesController.Reset();
            return "All Done! ^_^";
        }
    }
}
