﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VentripalApi.Models;

namespace VentripalApi.Controllers
{
    public class FaqController : ApiController
    {
        static Faq[] faqs = new Faq[]
            {
                new Faq(){ScreenNo =  1, Question = "How to add a billing fee agreement?",Answer = "Click New on the button bar to open the Managing Billing Fee Agreements window where you can add a new record."},
                new Faq(){ScreenNo =  1, Question = "How to edit a record?", Answer = "Double-click a record, select an item in the grid and click Edit on the button bar, or right-click an item in the grid and select Edit from the menu."},
                new Faq(){ScreenNo =  1, Question = "How to delete a record?", Answer = "Select an item in the grid and click Delete on the button bar, or right-click an item in the grid and select Delete from the menu."},
                new Faq(){ScreenNo =  2, Question = "How to Print the current view of the grid?", Answer = "Click Print on the button bar, or right-click the grid and select Print from the menu to print the entire grid."},
                new Faq(){ScreenNo =  2, Question = "How to Sort records?", Answer = "Click a column header to sort a column in ascending or descending order, or right-click the cell in the grid and select a sort option from the menu."},
                new Faq(){ScreenNo =  2, Question = "How to Select Columns?", Answer = "Click Select Columns on the button bar, or right-click a record on the grid and choose Select Columns from the menu. The Column Selection window displays."},
                new Faq(){ScreenNo =  3, Question = "How to assign a Service Code to a Billing Fee Agreement?", Answer = "In the Service Codes section, select items from the Available codes box and add them to the Assigned codes box."},
                new Faq(){ScreenNo =  3, Question = "How to Up a Billing Fee Agreement?", Answer = "Select New or Edit from the Billing Fee Agreements grid. Enter information into the various areas"},
            };


        [HttpGet]
        public IHttpActionResult GetFaq(int screenNo)
        {
            var selectedFaq = faqs.Where(x => x.ScreenNo == screenNo);
            if (selectedFaq == null)
                return NotFound();
            else
                return Ok(selectedFaq);
        }
    }
}
