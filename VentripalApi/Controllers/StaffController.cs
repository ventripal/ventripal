﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace VentripalApi.Controllers
{
    public class Staff
    {
        public string Name { get; set; }
    }
    public class StaffController : ApiController
    {
        List<Staff> staffList = new List<Staff>
        {
            new Staff { Name = "Reeti" },
            new Staff { Name = "Mo" },
            new Staff { Name = "Sanhita" },
            new Staff { Name = "Sean" },
        };

        public IHttpActionResult GetStaffs()
        {
            return Ok(staffList);
        }
    }
}