﻿using System.Collections.Generic;
using System.Web.Http;
using VentripalApi.Models;

namespace VentripalApi.Controllers
{
    public class WishesController : ApiController
    {
        private static List<Wish> _wishes = new List<Wish>();

        internal static void Reset()
        {
            _wishes = new List<Wish>();
        }

        static WishesController()
        {
            if (_wishes == null)
            {
                _wishes = new List<Wish>();
            }
        }
        public IEnumerable<Wish> Get()
        {
            return _wishes;
        }
        public void Post([FromBody]Wish wish)
        {
            _wishes.Add(wish);
        }
    }
}
