﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace VentripalApi.Controllers
{

    public class ClientBilling
    {
        public string Name { get; set; }
    }
    public class ClientBillingController : ApiController
    {
        static ConcurrentDictionary<string, ClientBilling> billingList = new ConcurrentDictionary<string, ClientBilling>();

        internal static void Reset()
        {
            billingList = new ConcurrentDictionary<string, ClientBilling>();
        }

        public IHttpActionResult GetBilling()
        {
            return Ok(billingList.Values.ToList());
        }

        [HttpPost]
        public IHttpActionResult PostBilling(ClientBilling newBilling)
        {
            if (newBilling != null)
            {
                billingList.TryAdd(newBilling.Name, newBilling);
                return Created<ClientBilling>(Request.RequestUri, newBilling);
            }
            else
                return Conflict();
        }
    }
}
