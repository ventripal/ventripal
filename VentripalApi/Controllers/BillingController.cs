﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace VentripalApi.Controllers
{

    public class Billing {
        public string BillingName { get; set; }
    }
    public class BillingController : ApiController
    {
        static ConcurrentDictionary<string, Billing> billingList = new ConcurrentDictionary<string, Billing>();

        internal static void Reset()
        {
            billingList = new ConcurrentDictionary<string, Billing>();
        }

        public IHttpActionResult GetBilling()
        {
            return Ok(billingList.Values.ToList());
        }

        [HttpPost]
        public IHttpActionResult PostBilling(Billing newBilling)
        {
            if (newBilling != null)
            {
                billingList.TryAdd(newBilling.BillingName, newBilling);
                return Created<Billing>(Request.RequestUri, newBilling);
            }
            else
                return Conflict();
        }
    }
}
