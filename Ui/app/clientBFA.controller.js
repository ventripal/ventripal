﻿(function () {
    'use strict';
    angular.module('app').controller('ClientBFAController',
        [
            'shared',
            function (shared) {
                var vm = this;

                vm.bfas = [];
                vm.shared = shared;
                vm.staffs = [];

                vm.canShow = canShow;
                vm.save = save;
                vm.shouldDisableSaveBFA = shouldDisableSaveBFA;
                vm.toggleCurrent = toggleCurrent;
                vm.amountChanged = amountChanged;

                activate();

                function activate() {
                    updateClientBFAs();
                    shared.getStaffs().then(getStaffsSuccess, getStaffsFailure);
                }

                function canShow() {
                    return shared.current === 'client';
                }
                function save() {
                    shared.saveBfa(vm.newBFA.selectedBFA.billingName).then(saveBfaSuccess, saveBfaFailure);
                }
                function toggleCurrent() {
                    shared.current = 'firm';
                    shared.togglePage();
                }
                function amountChanged(amount) {
                    shared.attention = amount > 5000;
                    if (shared.attention) {
                        $('.wiggle').ClassyWiggle('start', { degrees: ['10', '15', '10', '0', '-10', '-15', '-10', '0'], delay: 45 });
                    } else {
                        $('.wiggle').ClassyWiggle('stop');
                    }
                }
                function updateClientBFAs() {
                    shared.getClientBfas().then(getBfasSuccess, getBfasFailure);
                }

                function getBfasSuccess(result) {
                    vm.bfas = result.data;
                }

                function getBfasFailure(error) {
                    console.log('GetClientBfas error: ' + JSON.stringify(error));
                }

                function getStaffsSuccess(result) {
                    vm.staffs = result.data;
                }

                function getStaffsFailure(error) {
                    console.log('GetStaffs error: ' + JSON.stringify(error));
                }

                function saveBfaSuccess(result) {
                    updateClientBFAs();
                    vm.newBFA = {};
                }

                function saveBfaFailure(error) {
                    console.log('saveBFA error: ' + JSON.stringify(error));
                }

                function shouldDisableSaveBFA() {
                    return vm.shared.firmBFAs.length <= 0;
                }
            }
        ]);
}());