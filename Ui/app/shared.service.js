﻿(function () {
    'use strict';
    angular.module('app').factory('shared',
        function ($http) {
            var vm = this;
            vm.current = 'client';
            vm.firmBFAs = null;

            vm.getClientBfas = getClientBfas;
            vm.getStaffs = getStaffs;
            vm.CreateBfa = CreateBfa;
            vm.saveBfa = saveBfa;
            vm.GetBfa = GetBfa;
            vm.attention = false;
            vm.getFaq = getFaq;
            vm.togglePage = togglePage;

            vm.faqs = [];

            //var uri = 'http://localhost:5913';
            var uri = 'https://ventripalapi.azurewebsites.net';

            function togglePage() {
                vm.getFaq();
            }

            function getFaq() {
                var screenNo = 2;
                if (vm.current === 'client')
                    screenNo = parseInt('1');
                if (vm.current === 'firm')
                    screenNo = parseInt('2');

                return $http.get(uri + '/api/Faq?screenNo=' + screenNo).then(successCallbackGetFaq, errorCallback);
            }

            function successCallbackGetFaq(result) {
                console.log('success calling faq');
                vm.faqs = result.data;
            }
            function errorCallback() {
                console.log('get faq not successful');
            }
            function GetBfa() {
                return $http.get(uri + '/api/Billing').then(getBfaSuccess, getBfaFailure);
            }

            function getBfaSuccess(result) {
                if (vm.firmBFAs !== null && result.data.length === 1) {
                    toastr.info('You\'ve created a Billing Fee Agreement!');
                }
                vm.firmBFAs = result.data;
            }

            function getBfaFailure(error) {
                console.log('GetBfa error: ' + error);
            }

            function getClientBfas() {
                return $http.get(uri + '/api/ClientBilling');
            }

            function getStaffs() {
                return $http.get(uri + '/api/Staff');
            }

            function CreateBfa(name) {
                return $http.post(uri + '/api/Billing', { BillingName: name });
            }

            function saveBfa(name) {
                return $http.post(uri + '/api/ClientBilling', { Name: name });
            }

            return vm;
        }
    );
}());