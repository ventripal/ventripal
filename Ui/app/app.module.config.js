﻿(function () {
    'use strict';
    angular.module('app')
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            globalConfig
        ]);

    function globalConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('bfa', {
                views: {
                    'firmBFA': {
                        controller: 'FirmBFAController as vm',
                        templateUrl: 'app/firmBFA.html',
                    },
                    'clientBFA': {
                        controller: 'ClientBFAController as vm',
                        templateUrl: 'app/clientBFA.html',
                    },
                    'overlay': {
                        controller: 'overlayController as vm',
                        templateUrl: 'app/overlayView.html',
                    },
		},
                url: '/bfa',
            });
        $urlRouterProvider.otherwise('/bfa');
    }
}());