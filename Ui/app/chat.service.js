﻿(function () {
    'use strict';
    angular.module('app').factory('Chat', ['Hub', function (Hub) {
        var hub = new Hub('chat',
        {
            listeners: {
                'broadcastMessage': function(name, message) {
                    var encodedName = $('<div />').text(name).html();
                    var encodedMsg = $('<div />').text(message).html();
                }
            },
            methods: ['send']
        });
    }]);
}());