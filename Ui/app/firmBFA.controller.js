﻿(function () {
    'use strict';
    angular.module('app').controller('FirmBFAController',
        [
            'shared',
            function (shared) {
                var vm = this;

                vm.shared = shared;

                vm.canShow = canShow;
                vm.create = create;
                vm.toggleCurrent = toggleCurrent;

                activate();

                function activate() {
                    shared.GetBfa(true);
                }
                function canShow() {
                    return shared.current === 'firm';
                }
                function create() {
                    shared.CreateBfa(vm.bfaName).then(successCallback, errorCallback);
                }
                function toggleCurrent() {
                    shared.current = 'client';
                    shared.togglePage();
                }

                function successCallback(data) {
                    console.log("Success posting data!");
                    shared.GetBfa(false);
                    vm.bfaName = '';
                }

                function errorCallback(error) {
                    console.log("Error!");
                }
            }
        ]);
}());