﻿(function () {
    'use strict';
    angular.module('app').controller('overlayController', ['shared','$http',
    function (shared, $http) {
        var vm = this;
       
        vm.shared = shared;
        vm.shared.getFaq();
        vm.totd = getTip();
        vm.wiggle = function () {
            $('.wiggle').ClassyWiggle('start', { degrees: ['10', '15', '10', '0', '-10', '-15', '-10', '0'], delay: 45 });
        }
        vm.reset = function () {
            $('.wiggle').ClassyWiggle('stop');
        }
        vm.sendWishes = function (wishes) {
            vm.feedbackDisabled = true;
            $http.post('http://ventripalapi.azurewebsites.net/api/wishes',
                { user: 'testUser', starRating: '4', wishes: wishes, screenNo: 1 })
                .then(sendWishesComplete, sendWishesComplete);
        }
        function sendWishesComplete() {
            vm.wishes = "";
            vm.feedbackDisabled = false;
            wishesForm.$setPristine;

        }
        function getTip() {
            var tips = [
                'Don\'t forget to floss!',
                'Have you tried turning it off and on again?',
                'You can select multiple things at once with ctrl!',
                'It\'s all situational',
                'The Walking Dead is a paradigm for life',
                'Always look on the bright side of life',
                'Don\'t worry, be happy',
                'Speak softly, and carry a big stick',
                'Believe in yourself! You can do it!',
            ];
            var tipId = Math.floor((Math.random() * tips.length) + 1);
            return tips[tipId];
        }
    }]);
}());